import cv2
import numpy as np
import os

import ipdb

cv2.namedWindow('window', cv2.WINDOW_FREERATIO)
image = cv2.imread('test_pattern.jpg')

if os.path.exists('windowrect.npy'):
    windowrect = tuple(np.load('windowrect.npy'))
    x, y, w, h = windowrect
    cv2.moveWindow('window', x, y)
    cv2.resizeWindow('window', w, h)
else:
    windowrect = (0, 0, 300, 200)

counter = 0
while True:
    counter += 1
    x, y, w, h = windowrect
    cv2.namedWindow('window', cv2.WINDOW_FREERATIO)
    cv2.moveWindow('window', x, y)
    cv2.resizeWindow('window', w, h)

    resized = cv2.resize(image, (image.shape[1], int(image.shape[1] * h / w)))
    if counter % 30 < 15:
        resized = resized * 0
    cv2.imshow('window', resized)
    key = cv2.waitKey(33)
    if key >= 0:
        step = 1
        if key in [0, 1, 2, 3, 91, 47, 59, 39,]:
            if key == 0: # up
                y -= step
            if key == 1: # down
                y += step
            if key == 2: # left
                x -= step
            if key == 3: # right
                x += step

            if key == 91: # up
                h -= step
            if key == 47: # down
                h += step
            if key == 59: # left
                w -= step
            if key == 39: # right
                w += step

            windowrect = (x, y, w, h)
            print(windowrect)
            cv2.moveWindow('window', x, y)
            cv2.resizeWindow('window', w, h)
            np.save('windowrect.npy',windowrect)

        key = chr(key)
        if key == 'q':
            break

    # if rect != windowrect:
    #     windowrect = rect
    #     np.save('windowrect.npy',windowrect)

    # ipdb.set_trace()

    # handle = m.getid('window')
    # print(handle)

import cv2
import numpy as np
import json
import time
import os

import subprocess
from pythonosc import udp_client

from effect import Effecter
# import tqdm


class AudioPlayer:
    def __init__(self):
        self.proc = None
        pass

    def play(self, pos=0.0):
        self.stop()
        # devnull = open('/dev/null')
        # self.proc = subprocess.Popen(['play', 'sound_trimmed.mp3', 'trim', str(pos)], stderr=subprocess.DEVNULL, stdout=subprocess.DEVNULL)

    def stop(self):
        if self.proc:
            self.proc.kill()
            self.proc = None


def main():
    height = 384
    width = 768
    fps = 30
    t = 0
    tenmetsu = False
    tenmetsu_interval = 4 # 点滅周期
    radius = 30

    osc = udp_client.SimpleUDPClient("127.0.0.1", 5000)

    # cv2.namedWindow("window", cv2.WINDOW_NORMAL | cv2.WINDOW_FREERATIO)
    # if os.path.exists('windowrect.npy'):
    #     windowrect = tuple(np.load('windowrect.npy'))
    #     x, y, w, h = windowrect
    #     cv2.moveWindow('window', x, y)
    #     cv2.resizeWindow('window', w, h)
    # else:
    #     windowrect = (0, 0, 300, 200)

    data = json.load(open('data.json'))
    times = []

    for event in data:
        times.append(event['time'])

    times = np.array(times)
    dataIndex = 0
    player = AudioPlayer()
    effect = Effecter()
    # cap = cv2.VideoCapture('movie.mp4')

    # frame_count = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
    # fps = int(cap.get(cv2.CAP_PROP_FPS))


    def move(sec):
        nonlocal time_0
        time_0 -= sec
        t = time.time() - time_0
        player.play(t)

        print("{:02d}:{:02d}.{}".format(int(t//60), int(t%60), "{:.5f}".format(t - int(t)).split('.')[-1]))

    try:
        # 途中までにしておく
        duration = 60 * 10 # 10分
        time_0 = time.time()
        player.play()

        counter = 0

        while True:
            counter += 1
            t = time.time() - time_0
            # frame = np.zeros((height, width, 3), dtype=np.uint8)
            later = np.arange(len(times))[t < times]

            if len(later) == 0:
                break

            if t > duration:
                break

            dataIndex = later[0]
            event = data[dataIndex]

            # for osc
            x = event['x']
            y = event['y']

            osc.send_message("/pos", (x, y))

            # cap.set(0, )

            # cap.set(cv2.cv2.CAP_PROP_POS_MSEC, int(t * 1000))
            # ret, frame = cap.read()

            # x = int(event['x'] * width)
            # y = int(event['y'] * height)

            # margin = radius + 60

            # if x < margin:
            #     x = margin
            # if x > width - margin:
            #     x = width - margin
            # if y < margin:
            #     y = margin
            # if y > height - margin:
            #     y = height - margin

            # frame = cv2.circle(frame, (x, y), radius, (255, 255, 255), -1)

            # if tenmetsu and counter % tenmetsu_interval < tenmetsu_interval//2:
            #     frame = frame * 0

            # e = effect.step()

            # margin = 30
            # e2 = cv2.resize(e, (e.shape[1] - margin * 2, e.shape[0] - margin * 2))
            # e = e * 0
            # e[margin:-margin, margin:-margin] = e2

            # frame = ((frame.astype(float) + e.astype(float)))
            # frame[frame > 255] = 255
            # frame = frame.astype(np.uint8)

            # x, y, w, h = windowrect
            # cv2.moveWindow('window', x, y)
            # cv2.resizeWindow('window', w, h)
            # resized = cv2.resize(frame, (frame.shape[1], int(frame.shape[1] * h / w)))
            # cv2.imshow('window', frame)

            key = cv2.waitKey(33)
            if key >= 0:
                effect.onKey(key)
                key = chr(key)
                if key == 'q':
                    break
                if key == 'a':
                    move(-10)
                if key == 's':
                    move(10)
                # if key == 't': # 点滅
                #     tenmetsu = not tenmetsu

    except:
        import traceback
        traceback.print_exc()
    player.stop()

main()

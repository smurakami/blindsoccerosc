import cv2
import numpy as np
import json

# データをnumpy化する


def main():
    width = 640
    height = 320
    fps = 30
    time = 0

    data = json.load(open('data.json'))

    times = []

    for event in data:
        times.append(event['time'])

    times = np.array(times)

    dataIndex = 0

    # 途中までにしておく
    duration = 60 * 10 # 10分

    output = []

    while True:
        print("{:.2f}/{:.2f}".format(time, duration))
        later = np.arange(len(times))[time < times]

        if len(later) == 0:
            break

        if time > duration:
            break

        dataIndex = later[0]
        event = data[dataIndex]

        x = event['x']
        y = event['y']

        print(x, y)

        output.append([y, x])


        time += 1.0 / fps

    output = np.array(output)

    np.save('data.npy', output)


main()

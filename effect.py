import cv2
import numpy as np
import sys


cv2.namedWindow("window", cv2.WINDOW_NORMAL | cv2.WINDOW_FREERATIO)


def sigmoid(x):
   return 1 / (1 + np.exp( -x ) )

def smoothstep_zero(x, smooth):
    y = np.zeros_like(x)
    y = np.sin(x * np.pi / (2 * smooth)) / 2 + 0.5
    y[x < -smooth] = 0
    y[x > smooth] = 1
    return y


def smoothstep(x, th, smooth):
    # return sigmoid((x - th) / smooth)
    return smoothstep_zero(x - th, smooth)


def step(x, th):
    return (x > th).astype(float)


def rect(shape, pos, r, smooth):
    if smooth > 0:
        s = lambda x, th: smoothstep(x, th, smooth)
    else:
        s = step

    ii, jj = np.indices(shape)
    y, x = pos
    horizontal = 1 - s(np.abs(y - ii), r)
    vertical = 1 - s(np.abs(x - jj), r)
    return horizontal * vertical

def circle(shape, pos, r, smooth=0.5):
    ii, jj = np.indices(shape)
    y, x = pos
    dist = np.sqrt((ii - y) ** 2 + (jj - x) ** 2)

    return 1 - smoothstep(dist, r, smooth)


def rect_line(shape, pos, r, smooth=0):
    return rect(shape, pos, r + smooth/2, smooth) - rect(shape, pos, r - 1 - smooth/2, smooth)
    


class Blinker():
    # 点滅してコートの縁を教えてくれる
    def __init__(self):
        self.counter = 0

    def update(self):
        self.counter += 1

    def draw(self):
        pattern = [1, 1, 1, 0]
        time = 30
        index = int(self.counter / time * len(pattern)) % len(pattern)

        val = pattern[index]

        pixel = np.zeros((16, 32))
        pixel[ 0] = val
        pixel[-1] = val
        pixel[:,  0] = val
        pixel[:, -1] = val

        return pixel


class MoveGuide():
    # 点がぐるぐるまわって位置を教えてくれる
    def __init__(self):
        self.counter = 0

        x, y = 0, 0
        path = []
        while y < 15:
            path.append([y, x])
            y += 1

        while x < 31:
            path.append([y, x])
            x += 1

        while y > 0:
            path.append([y, x])
            y -= 1

        while x > 0:
            path.append([y, x])
            x -= 1

        self.path = np.array(path)

    def update(self):
        self.counter += 1

    def draw(self):
        time = 60
        length = 1
        index = int(self.counter / time * len(self.path)) % len(self.path)

        pixel = np.zeros((16, 32))
        for i in range(length):
            p = tuple(self.path[(index - i) % len(self.path)])
            pixel[p] = 1

        return pixel


class Possetion():
    # ポゼッションをおしえてくれるやつ
    def __init__(self):
        self.counter = 0
        self.units = []

    def update(self):
        self.counter += 1

        if self.counter % 15 == 0:
            self.units.append(self.Unit())

        for unit in self.units:
            unit.update()

        self.units = list(filter(self.Unit.isActive, self.units))

    def draw(self):
        pixel = np.zeros((16, 32))
        speed = 1.5

        for unit in self.units:
            x = int(unit.counter * speed)
            length = 1
            pixel[ 0, x-length:x] = 1
            pixel[-1, x-length:x] = 1

        return pixel


    class Unit():
        def __init__(self):
            self.counter = 0

        def update(self):
            self.counter += 1

        def isActive(self):
            length = 6
            return self.counter <= 32 + length



class BallPos():
    # ボール位置の可視化
    def __init__(self):
        self.data = np.load('data.npy')
        self.counter = 0

    def update(self):
        self.counter += 1

    def draw(self):
        pixel = np.zeros((16, 32))
        radius = 1.5
        pos = self.data[self.counter % len(self.data)]
        pos = (pos * np.array([16, 32]))

        y, x = np.indices(pixel.shape)
        dist = np.sqrt((x - pos[1]) ** 2 + (y - pos[0]) ** 2)
        pixel += circle(pixel.shape, pos, radius)

        return pixel


class BlinkingBallPos():
    # ボール位置の可視化
    def __init__(self):
        self.data = np.load('data.npy')
        self.counter = 0
        self.posettion = True

    def update(self):
        self.counter += 1

    def draw(self):
        if self.posettion:
            time = 15
            pattern = [1, 1, 1, 0]
        else:
            time = 20
            pattern = [1, 1, 0, 1, 1, 0, 0, 0]
        index = int(self.counter / time * len(pattern)) % len(pattern)
        val = pattern[index]

        pixel = np.zeros((16, 32))
        radius = 1.5
        pos = self.data[self.counter % len(self.data)]
        pos = (pos * np.array([16, 32]))

        y, x = np.indices(pixel.shape)
        dist = np.sqrt((x - pos[1]) ** 2 + (y - pos[0]) ** 2)
        pixel += circle(pixel.shape, pos, radius)

        pixel *= val

        return pixel

    def onkey(self, key):
        self.posettion = not self.posettion


class Effecter():
    def __init__(self):
        self.objects = []
        self.mirror = False
        # self.objects.append(Blinker())
        # self.objects.append(MoveGuide())
        # self.objects.append(Possetion())
        # self.objects.appenyoutube-dl-mp4='youtube-dl -f 22/18/136/mp4/best'd(BlinkingBallPos())

        # self.run()

    def run(self):
        while True:
            self.update()
            pixel = self.draw()
            image = self.finalizePixel(pixel)
            cv2.imshow("window", image)
            key = cv2.waitKey(33)

            if key >= 0:
                for elem in self.objects:
                    if hasattr(elem, 'onkey'):
                        elem.onkey(key)
            if key == ord('q'):
                break

    def step(self):
        self.update()
        pixel = self.draw()
        image = self.finalizePixel(pixel)
        return image

    def onKey(self, key):
        for elem in self.objects:
            if hasattr(elem, 'onkey'):
                elem.onkey(key)

        if key >= 0:
            key = chr(key)
            # if key == 'b':
            #     self.objects = []
            #     self.objects.append(Blinker())
            if key == 'm':
                self.objects = []
                self.objects.append(MoveGuide())
            if key == 'p':
                self.objects = []
                self.objects.append(Possetion())
            if key == 'l':
                # landscape mirror
                self.mirror = not self.mirror
            if key == 'n':
                self.objects = []

    def update(self):
        for elem in self.objects:
            elem.update()

    def draw(self):
        pixel = np.zeros((16, 32))
        for elem in self.objects:
            pixel += elem.draw()

        pixel[pixel > 1] = 1
        pixel[pixel < 0] = 0


        if self.mirror:
            pixel = pixel[:, ::-1]

        return pixel

    def getFinalizeParams(self):
        radius = 12
        padding = 0
        margin = 0
        grid = (radius + padding) * 2

        return radius, padding, margin, grid

    def getFinalizeImageSize(self):
        radius, padding, margin, grid = self.getFinalizeParams()
        height, width = 16, 32
        return (grid * height + margin * 2, grid * width + margin * 2)

    def finalizePixel(self, pixel):
        radius, padding, margin, grid = self.getFinalizeParams()
        height, width = pixel.shape
        image = np.zeros((*self.getFinalizeImageSize(), 3), dtype=float)
        # color = np.array([231, 76, 60])[..., ::-1] / 255
        color = np.array([255, 255, 255])[..., ::-1] / 255
        for i in range(height):
            for j in range(width):
                y = int(i * grid + grid/2 + margin)
                x = int(j * grid + grid/2 + margin)
                val = pixel[i, j]
                image = cv2.circle(image, (x, y), radius, color * val, -1, 16)

        image = (image * 255).astype(np.uint8)

        return image


# Main()
